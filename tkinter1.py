import tkinter
import time

window_width = 800

window_height = 600

ball_start_xpos = 50

ball_start_ypos = 50

ball_radius = 30

ball_movement = 5

refresh_seconds = 0.01


def create_window():
    window = tkinter.Tk()
    window.title("Bouncing Ball")
    # Uses python 3.6+ string interpolation
    window.geometry(f'{window_width}x{window_height}')
    return window


def create_canvas(window):
    canvas = tkinter.Canvas(window)
    canvas.configure(bg="light blue")
    canvas.pack(fill="both", expand=True)
    return canvas


def animate_ball(window, canvas, xinc, yinc):
    ball = canvas.create_oval(ball_start_xpos - ball_radius,
                              ball_start_ypos - ball_radius,
                              ball_start_xpos + ball_radius,
                              ball_start_ypos + ball_radius,
                              fill="blue", outline="blue", width=4)
    while True:
        canvas.move(ball, xinc, yinc)
        window.update()
        time.sleep(refresh_seconds)
        ball_pos = canvas.coords(ball)
        # unpack array to variables
        xl, yl, xr, yr = ball_pos
        if xl < abs(xinc) or xr > window_width - abs(xinc):
            xinc = -xinc
        if yl < abs(yinc) or yr > window_height - abs(yinc):
            yinc = -yinc


animation_window = create_window()
animation_canvas = create_canvas(animation_window)
animate_ball(animation_window, animation_canvas, ball_movement, ball_movement)